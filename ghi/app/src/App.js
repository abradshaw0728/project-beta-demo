import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import TechniciansList from './TechniciansList';
import TechniciansForm from './TechniciansForm';
import AppointmentsForm from './AppointmentsForm';
import AppointmentsHistory from './AppointmentsHistory';
import AppointmentsList from './AppointmentsList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/create-manufacturer" element={<ManufacturerForm />} />
          <Route path="/vehicle-models" element={<VehicleModelList />} />
          <Route path="/create-vehicle-model" element={<VehicleModelForm />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/create-automobile" element={<AutomobileForm />} />
          <Route path="/technicians" element={<TechniciansList />} />
          <Route path="/technicians/create" element={<TechniciansForm />} />
          <Route path="/appointments/create" element={<AppointmentsForm />} />
          <Route path="/appointments/history" element={<AppointmentsHistory />} />
          <Route path="/appointments" element={<AppointmentsList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
