import React, { useState, useEffect } from 'react';

function AppointmentsHistory() {
  const [vinSearch, setVinSearch] = useState('');
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log('Search VIN:', vinSearch);

    const filtered = appointments.filter((appointment) =>
      appointment.vin.includes(vinSearch)
    );
    setFilteredAppointments(filtered);
  };

  useEffect(() => {
    const fetchAppointments = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (!response.ok) {
          throw new Error('Failed to fetch appointments');
        }
        const data = await response.json();

        const appointmentsWithTechnicianNames = data.appointments.map(appointment => ({
          ...appointment,
          technician: `${appointment.technician.first_name} ${appointment.technician.last_name}`,
        }));

        setAppointments(appointmentsWithTechnicianNames);
        setFilteredAppointments(appointmentsWithTechnicianNames);
      } catch (error) {
        console.error('Error fetching appointments:', error.message);
      }
    };

    fetchAppointments();
  }, []);

  return (
    <div>
      <h2>Service History</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="vinSearch" className="form-label">
            Search by VIN
          </label>
          <input
            type="text"
            id="vinSearch"
            className="form-control"
            value={vinSearch}
            onChange={(e) => setVinSearch(e.target.value)}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Search
        </button>
      </form>

      <table className="table table-striped mt-4">
        <thead>
          <tr>
            <th>VIN</th>
            <th>VIP Status</th>
            <th>Customer Name</th>
            <th>Date and Time</th>
            <th>Technician</th>
            <th>Reason for Service</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment, index) => (
            <tr key={index}>
              <td>{appointment.vin}</td>
              <td>{appointment.vipStatus ? 'VIP' : 'Regular'}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentsHistory;
