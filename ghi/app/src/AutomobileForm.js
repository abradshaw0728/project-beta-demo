import React, { useState } from "react";

function AutomobileForm() {
  const [automobileData, setAutomobileData] = useState({
    color: "",
    year: "",
    vin: "",
    sold: false,
    model_id: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setAutomobileData({
      ...automobileData,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch("/api/automobiles/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(automobileData),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Automobile created:", data);
        setAutomobileData({
          color: "",
          year: "",
          vin: "",
          sold: false,
          model_id: "",
        });
      })
      .catch((error) => {
        console.error("Error creating automobile:", error);
      });
  };

  return (
    <div>
      <h2>Create a New Automobile</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="color">Color:</label>
          <input
            type="text"
            id="color"
            name="color"
            value={automobileData.color}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="year">Year:</label>
          <input
            type="number"
            id="year"
            name="year"
            value={automobileData.year}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="vin">VIN:</label>
          <input
            type="text"
            id="vin"
            name="vin"
            value={automobileData.vin}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="sold">Sold:</label>
          <input
            type="checkbox"
            id="sold"
            name="sold"
            checked={automobileData.sold}
            onChange={(e) =>
              setAutomobileData({
                ...automobileData,
                sold: e.target.checked,
              })
            }
          />
        </div>
        <div>
          <button type="submit">Create Automobile</button>
        </div>
      </form>
    </div>
  );
}

export default AutomobileForm;
