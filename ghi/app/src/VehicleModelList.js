import React, { useState, useEffect } from "react";

function VehicleModelList() {
  const [vehicleModels, setVehicleModels] = useState([]);

  useEffect(() => {
    fetch("/api/vehicle_models/")
      .then((response) => response.json())
      .then((data) => setVehicleModels(data.models))
      .catch((error) => console.error("Error fetching vehicle models", error));
  }, []);

  return (
    <div>
      <h1>Vehicle Model List</h1>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Picture URL</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModels.map((model) => (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td>{model.picture_url}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default VehicleModelList;
